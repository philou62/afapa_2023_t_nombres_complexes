package NombreComplexe;

public class Complexe {
	private double pReelle;
	private double pImaginaire;
	private double cosTeta;
	private double sinTeta;
	
	// constructeur pour initialiser les variables
	public Complexe() {
		this.pReelle = 0.0;
		this.pImaginaire = 0.0;
	}
	
	// deuxieme constructeur 'surcharge du constructeur
	public Complexe(double a, double b) {
		this.pReelle = a;
		this.pImaginaire = b;
	}
	
	// getter et setter ici que les getter sont utiles

	public double getpReelle() {
		return pReelle;
	}

	public double getpImaginaire() {
		return pImaginaire;
	}
	
	// Calculer le modue
	public double Module() {
		//return (this.pReelle * this.pReelle + this.pImaginaire * this.pImaginaire);
		return Math.pow(pReelle, 2) + Math.pow(pImaginaire, 2);
	}
	
	// Argument
	public void argument() {
		this.cosTeta = this.pReelle / Module();
		this.sinTeta = this.pImaginaire / Module();
	}

	public Complexe sommeComplexe(Complexe c1, Complexe c2) {
		Complexe c3 = new Complexe();
		c3.pReelle = c1.pReelle + c2.pReelle;
		c3.pImaginaire = c1.pImaginaire + c2.pImaginaire;
		return c3;
		}
	
	public Complexe produitComplexe(Complexe c1, Complexe c2) {
		Complexe c3 = new Complexe();
		c3.pReelle = (c1.pReelle * c2.pReelle) - (c1.pImaginaire * c2.pImaginaire);
		c3.pImaginaire = (c1.pReelle * c2.pImaginaire)+(c1.pImaginaire * c2.pReelle);
		return c3;
		}
	
	
	
}
