package NombreComplexe;

public class DemoComplexe {

	public static void main(String[] args) {
		Complexe c1 = new Complexe(22.0, 36.0);
		Complexe c2 = new Complexe(12.0, 15.0);
		Complexe c3 = new Complexe();
		System.out.println(c3.getpReelle());

		System.out.println(c2.getpReelle());
		System.out.println(c2.getpImaginaire());
		System.out.println(c2.Module());
		System.out.println("la somme de la partie reellé c2 est => " + c2.sommeComplexe(c1, c2).getpReelle());
		System.out.println("Le produit de la partie reelle c2 est => " + c2.produitComplexe(c1, c2).getpReelle());
		System.out.println("La somme de la partie imaginaire c2 est => " + c2.produitComplexe(c1, c2).getpImaginaire());
	}

}
